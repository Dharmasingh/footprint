'use strict';
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const port = process.env.PORT || 3000;
var mysql = require('mysql');

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//app.use(express.static(__dirname + '../client/dist/client'));


var mysqlConnection  = mysql.createConnection({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : 'foot'
  });


app.listen(port, () => {
    console.log(`Server is listening at port ${port}`);
});

app.get('/', (req, res) => {
	res.send('HEY! Welcome to Plant Your Foot Print')
})

//Get all issues
app.get('/products',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query('SELECT * FROM products ', (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});

//Get all issues
app.get('/products/:id',(req,res)=>{
    //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
    mysqlConnection.query('SELECT * FROM products WHERE id = ?',[req.params.id], (err, rows)=>{
      if(!err)
      res.send(rows);
      else
      console.log(err);
  })
});

//Get Western Himalaya
app.get('/western-himalaya',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query(`SELECT * FROM products WHERE zone IN ('Western Himalaya') `, (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});

//Get Plains of Punjab
app.get('/plains-of-punjab',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query(`SELECT * FROM products WHERE zone IN ('Plains of Punjab') `, (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});

//Get Arid Western Zone
app.get('/arid-western-zone',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query(`SELECT * FROM products WHERE zone IN ('Arid Western Zone') `, (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});

//Get Urban Centres
app.get('/urban-centres',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query(`SELECT * FROM products WHERE zone IN ('Urban Centres') `, (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});

//Get Western Ghats
app.get('/western-ghats',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query(`SELECT * FROM products WHERE zone IN ('Western Ghats') `, (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});

//Get Ganetic Plains
app.get('/ganetic-plains',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query(`SELECT * FROM products WHERE zone IN ('Ganetic Plains') `, (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});

//Get Central India
app.get('/central-india',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query(`SELECT * FROM products WHERE zone IN ('Central India') `, (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});

//Get Deccan Plateau
app.get('/deccan-plateau',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query(`SELECT * FROM products WHERE zone IN ('Deccan Plateau') `, (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});

//Get Eastern Himalaya and North East India
app.get('/eastern-himalaya',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query(`SELECT * FROM products WHERE zone IN ('Eastern Himalaya') `, (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});

//Get Eastern Plains
app.get('/eastern-plains',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query(`SELECT * FROM products WHERE zone IN ('Eastern Plains') `, (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});

//Get Eastern Ghats
app.get('/eastern-ghats',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query(`SELECT * FROM products WHERE zone IN ('Eastern Ghats') `, (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});

//Get Southern Plains
app.get('/southern-plains',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query(`SELECT * FROM products WHERE zone IN ('Southern Plains') `, (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});

//Get Southern Plains
app.get('/planted-trees',(req,res)=>{
      //mysqlConnection.query('SELECT * FROM products ORDER BY id DESC ', (err, rows)=>{
	  mysqlConnection.query(`SELECT sum(planted_trees) AS totTree FROM products `, (err, rows)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
});