import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ProductService } from '../product.service';
import { Observable } from 'rxjs';
declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [ProductService]
})
export class HomeComponent implements OnInit {

  products_details;

  oldItems:any;
  oldItemsCount;
  oldItemList;


  public cement: number;
  public steel: number;
  public bricks: number;
  public tiles: number;
  public glass: number;
  public aluminium: number;
  public totalTons: number;
  public plantedTrees: number;

  public sustainable: number;
  public hybrid: number;
  public conventional: number;
  public totalTons1: number;
  public plantedTrees1: number

  constructor(private productService: ProductService) { }

  calculateCarbon(){
    this.totalTons = undefined;
    this.totalTons1 = undefined;
    this.totalTons = ((this.cement*400) + (this.steel*600) + (this.bricks*5) + (this.tiles*10) + (this.glass*500) + (this.aluminium*700));
    this.plantedTrees = (this.totalTons/10);
  }
  calculateCarbon1(){
    this.totalTons = undefined;
    this.totalTons1 = undefined;
    this.totalTons1 = ((this.sustainable*300)+(this.hybrid*500)+(this.conventional));
    this.plantedTrees1 = (this.totalTons1/10);
  }

  clearButton(){
    this.cement = null,
    this.steel = null,
    this.bricks = null,
    this.tiles = null,
    this.glass = null,
    this.aluminium = null,
    this.totalTons = null,
    this.plantedTrees = null,
    this.sustainable = null,
    this.hybrid = null,
    this.conventional = null,
    this.totalTons1 = null,
    this.plantedTrees1 = null
  }




//Filter By Category
getRegion1(){
  this.productService.getWesternHimalaya().subscribe(data =>{
    console.log("Western Himalaya");
    this.products_details= data;
    console.log(this.products_details);
  })
}

getRegion2(){
  this.productService.getPlainofPunjab().subscribe(data =>{
    console.log("Plains of Punjab");
    this.products_details= data;
    console.log(this.products_details);
  })
}

getRegion3(){
  this.productService.getAridWesternZone().subscribe(data =>{
    console.log("Arid Western Zone");
    this.products_details= data;
    console.log(this.products_details);
  })
}

getRegion4(){
  this.productService.getUrbanCentres().subscribe(data =>{
    console.log("Urban Centres");
    this.products_details= data;
    console.log(this.products_details);
  })
}

getRegion5(){
  this.productService.getWesternGhats().subscribe(data =>{
    console.log("Western Ghats");
    this.products_details= data;
    console.log(this.products_details);
  })
}

getRegion6(){
  this.productService.getGaneticPlains().subscribe(data =>{
    console.log("Ganetic Plains");
    this.products_details= data;
    console.log(this.products_details);
  })
}

getRegion7(){
  this.productService.getCentralIndia().subscribe(data =>{
    console.log("Central India");
    this.products_details= data;
    console.log(this.products_details);
  })
}

getRegion8(){
  this.productService.getDeccanPlateau().subscribe(data =>{
    console.log("Deccan Plateau");
    this.products_details= data;
    console.log(this.products_details);
  })
}

getRegion9(){
  this.productService.getEasternHimalaya().subscribe(data =>{
    console.log("Eastern Himalaya and North East India");
    this.products_details= data;
    console.log(this.products_details);
  })
}

getRegion10(){
  this.productService.getEasternPlains().subscribe(data =>{
    console.log("Eastern Plains");
    this.products_details= data;
    console.log(this.products_details);
  })
}

getRegion11(){
  this.productService.getEasternGhats().subscribe(data =>{
    console.log("Eastern Ghats");
    this.products_details= data;
    console.log(this.products_details);
  })
}

getRegion12(){
  this.productService.getSouthernPlains().subscribe(data =>{
    console.log("Southern Plains");
    this.products_details= data;
    console.log(this.products_details);
  })
}


// addToCart(index){
//   this.productService.getProducts().subscribe(data =>
//     {
//       this.products_details= data;
//       console.log(this.products_details[index]);
//     })
//     localStorage.setItem('Cart Items', JSON.stringify(this.products_details[index]));
//     console.log("One Item Added");
// }

addToCart(index){
  this.productService.getProducts().subscribe(data =>
    {
      var oldItems = JSON.parse(localStorage.getItem('itemsArray')) || [];
      this.products_details= data;
      console.log(this.products_details[index]);
      var newItem = this.products_details[index];
      oldItems.push(newItem);
      console.log("One Item Added");
      localStorage.setItem('itemsArray', JSON.stringify(oldItems));
    })
    window.alert("Product added Successfully !");
    
}



ngAfterViewInit() {
    $(document).ready(function () {
      var cement1, steel_1, bricks1, tiles1, glass1,aluminium1,eco,hybrid,conventional;
      var final, numOfTrees;
      var kgPerCementBag = 907.185;
      var kgPerSteel = 1814.37;
      var kgPerBrick =0.7;
      var ftTOmtPerTile = 1.532;
      var ftTOmtPerGlass=1.672254;
      var kgPerAluminium = 907.185;
      var kgPerEco  = 25;
      var kgPerHybrid  = 50;
      var kgPerConventional = 70;

      $(".cement").on('keyup keydown keypress', function () {
        $("#eco").val("");
        $("#hybrid").val("");
        $("#conventional").val("");

        if (isNaN(parseFloat($(".cement").val()))) {
          cement1 = 0;
        }

        else {
          cement1 = parseFloat(($(".cement").val())) * kgPerCementBag;
        }

        if (isNaN(parseFloat($(".steel").val()))) {
          steel_1 = 0;
        }

        else {
          steel_1 = parseFloat($(".steel").val()) *kgPerSteel;
        }

        if (isNaN(parseFloat($(".bricks").val()))) {
          bricks1 = 0;
        }

        else {
           bricks1 = parseFloat($(".bricks").val())*kgPerBrick; 
          }

        if (isNaN(parseFloat($(".tiles").val()))) {
          tiles1 = 0;
        }

        else {
          tiles1 = parseFloat($(".tiles").val()) * ftTOmtPerTile; 
        }

        if (isNaN(parseFloat($(".glass").val()))) {
          glass1 = 0;
        }

        else {
          glass1 = parseFloat($(".glass").val()) * ftTOmtPerGlass; 
        }

        if (isNaN(parseFloat($(".aluminium").val()))) {
          aluminium1 = 0;
        }

        else {
          aluminium1 = parseFloat($(".aluminium").val()) * kgPerAluminium; 
        }

        final = ((steel_1 + cement1 + bricks1 + tiles1 +glass1 +aluminium1)/907.185).toPrecision(3);
        numOfTrees = (steel_1 + cement1 + bricks1 + tiles1 +glass1 +aluminium1)/10;

        $('#co2').val(final);
        $('#trees').val(parseInt(numOfTrees));

        // $(".display").text(final);
        // $(".tree").text(parseInt(numOfTrees))
        
      });





      $(".steel").on('keyup keydown keypress', function () {
        $("#eco").val("");
        $("#hybrid").val("");
        $("#conventional").val("");
        

        if (isNaN(parseFloat($(".cement").val()))) {
          cement1 = 0;
        }

        else {
          cement1 = parseFloat($(".cement").val()) * kgPerCementBag;
        }

        if (isNaN(parseFloat($(".steel").val()))) {
          steel_1 = 0;
        }

        else {
          steel_1 = parseFloat($(".steel").val())*kgPerSteel;
        }

        if (isNaN(parseFloat($(".bricks").val()))) {
          bricks1 = 0;
        }

        else {
           bricks1 = parseFloat($(".bricks").val())*kgPerBrick; 
          }

        if (isNaN(parseFloat($(".tiles").val()))) {
          tiles1 = 0;
        }

        else { 
          tiles1 = parseFloat($(".tiles").val()) *ftTOmtPerTile;
         }

         if (isNaN(parseFloat($(".glass").val()))) {
          glass1 = 0;
        }

        else {
          glass1 = parseFloat($(".glass").val()) * ftTOmtPerGlass; 
        }
        if (isNaN(parseFloat($(".aluminium").val()))) {
          aluminium1 = 0;
        }

        else {
          aluminium1 = parseFloat($(".aluminium").val()) * kgPerAluminium; 
        }
        final =( (steel_1 + cement1 + bricks1 + tiles1 + glass1 + aluminium1)/907.185).toPrecision(3);
        numOfTrees = (steel_1 + cement1 + bricks1 + tiles1 +glass1 +aluminium1)/10;

        $('#co2').val(final);
        $('#trees').val(parseInt(numOfTrees));

      });





      $(".bricks").on('keyup keydown keypress', function () {
        $("#eco").val("");
        $("#hybrid").val("");
        $("#conventional").val("");
     
        if (isNaN(parseFloat($(".cement").val()))) {
          cement1 = 0;
        }

        else {
          cement1 = parseFloat($(".cement").val()) * kgPerCementBag;
        }

        if (isNaN(parseFloat($(".steel").val()))) {
          steel_1 = 0;
        }

        else {
          steel_1 = parseFloat($(".steel").val())*kgPerSteel;
        }

        if (isNaN(parseFloat($(".bricks").val()))) {
          bricks1 = 0;
        }

        else {
          bricks1 = parseFloat($(".bricks").val())*kgPerBrick;
        }

        if (isNaN(parseFloat($(".tiles").val()))) {
          tiles1 = 0;
        }

        else {
          tiles1 = parseFloat($(".tiles").val())*ftTOmtPerTile ;
        }

        if (isNaN(parseFloat($(".glass").val()))) {
          glass1 = 0;
        }

        else {
          glass1 = parseFloat($(".glass").val()) * ftTOmtPerGlass; 
        }
        if (isNaN(parseFloat($(".aluminium").val()))) {
          aluminium1 = 0;
        }

        else {
          aluminium1 = parseFloat($(".aluminium").val()) * kgPerAluminium; 
        }
        final = ((steel_1 + cement1 + bricks1 + tiles1 +glass1 +aluminium1)/907.185).toPrecision(3);
        numOfTrees = (steel_1 + cement1 + bricks1 + tiles1 +glass1 +aluminium1)/10;

        $('#co2').val(final);
        $('#trees').val(parseInt(numOfTrees));

      });





      $(".tiles").on('keyup keydown keypress', function () {
        $("#eco").val("");
        $("#hybrid").val("");
        $("#conventional").val("");
     

        if (isNaN(parseFloat($(".cement").val()))) {
          cement1 = 0;
        }

        else {
          cement1 = parseFloat($(".cement").val()) * kgPerCementBag;
        }

        if (isNaN(parseFloat($(".steel").val()))) {
          steel_1 = 0;
        }

        else {
          steel_1 = parseFloat($(".steel").val())*kgPerSteel;

        }

        if (isNaN(parseFloat($(".bricks").val()))) {
          bricks1 = 0;

        }

        else {
          bricks1 = parseFloat($(".bricks").val())*kgPerBrick;
        }

        if (isNaN(parseFloat($(".tiles").val()))) {
          tiles1 = 0;

        }
        else {
          tiles1 = parseFloat($(".tiles").val()) *ftTOmtPerTile;
        }
        if (isNaN(parseFloat($(".glass").val()))) {
          glass1 = 0;
        }

        else {
          glass1 = parseFloat($(".glass").val()) * ftTOmtPerGlass; 
        }
        if (isNaN(parseFloat($(".aluminium").val()))) {
          aluminium1 = 0;
        }

        else {
          aluminium1 = parseFloat($(".aluminium").val()) * kgPerAluminium; 
        }
        final = ((steel_1 + cement1 + bricks1 + tiles1 +glass1 +aluminium1)/907.185).toPrecision(3);
        numOfTrees = (steel_1 + cement1 + bricks1 + tiles1 +glass1 +aluminium1)/10;

        $('#co2').val(final);
        $('#trees').val(parseInt(numOfTrees));

      });


      $(".glass").on('keyup keydown keypress', function () {
        $("#eco").val("");
        $("#hybrid").val("");
        $("#conventional").val("");
      

        if (isNaN(parseFloat($(".cement").val()))) {
          cement1 = 0;
        }

        else {
          cement1 = parseFloat($(".cement").val()) * kgPerCementBag;
        }

        if (isNaN(parseFloat($(".steel").val()))) {
          steel_1 = 0;
        }

        else {
          steel_1 = parseFloat($(".steel").val())*kgPerSteel;

        }

        if (isNaN(parseFloat($(".bricks").val()))) {
          bricks1 = 0;

        }

        else {
          bricks1 = parseFloat($(".bricks").val())*kgPerBrick;
        }

        if (isNaN(parseFloat($(".tiles").val()))) {
          tiles1 = 0;

        }
        else {
          tiles1 = parseFloat($(".tiles").val()) *ftTOmtPerTile;
        }
        if (isNaN(parseFloat($(".glass").val()))) {
          glass1 = 0;
        }

        else {
          glass1 = parseFloat($(".glass").val()) * ftTOmtPerGlass; 
        }
        if (isNaN(parseFloat($(".aluminium").val()))) {
          aluminium1 = 0;
        }

        else {
          aluminium1 = parseFloat($(".aluminium").val()) * kgPerAluminium; 
        }
        final = ((steel_1 + cement1 + bricks1 + tiles1 +glass1 +aluminium1)/907.185).toPrecision(3);
        numOfTrees = (steel_1 + cement1 + bricks1 + tiles1 +glass1 +aluminium1)/10;

        $('#co2').val(final);
        $('#trees').val(parseInt(numOfTrees));

      });


      $(".aluminium").on('keyup keydown keypress', function () {
        $("#eco").val("");
        $("#hybrid").val("");
        $("#conventional").val("");
        

        if (isNaN(parseFloat($(".cement").val()))) {
          cement1 = 0;
        }

        else {
          cement1 = parseFloat($(".cement").val()) * kgPerCementBag;
        }

        if (isNaN(parseFloat($(".steel").val()))) {
          steel_1 = 0;
        }

        else {
          steel_1 = parseFloat($(".steel").val())*kgPerSteel;

        }

        if (isNaN(parseFloat($(".bricks").val()))) {
          bricks1 = 0;

        }

        else {
          bricks1 = parseFloat($(".bricks").val())*kgPerBrick;
        }

        if (isNaN(parseFloat($(".tiles").val()))) {
          tiles1 = 0;

        }
        else {
          tiles1 = parseFloat($(".tiles").val()) *ftTOmtPerTile;
        }
        if (isNaN(parseFloat($(".glass").val()))) {
          glass1 = 0;
        }

        else {
          glass1 = parseFloat($(".glass").val()) * ftTOmtPerGlass; 
        }
        if (isNaN(parseFloat($(".aluminium").val()))) {
          aluminium1 = 0;
        }

        else {
          aluminium1 = parseFloat($(".aluminium").val()) * kgPerAluminium; 
        }
        final = ((steel_1 + cement1 + bricks1 + tiles1 +glass1 +aluminium1)/907.185).toPrecision(3);
        numOfTrees = (steel_1 + cement1 + bricks1 + tiles1 +glass1 +aluminium1)/10;

        $('#co2').val(final);
        $('#trees').val(parseInt(numOfTrees));

      });



      //area
      $(".eco").on('keyup keydown keypress', function () {
        //alert("called");
        $("#cement").val("");
        $("#steel").val("");
        $("#bricks").val("");
        $("#glass").val("");
        $("#tiles").val("");
        $("#aluminium").val("");
        
        if (isNaN(parseFloat($(".eco").val()))) {
          eco = 0;
        }

        else {
          eco = parseFloat($(".eco").val()) * kgPerEco; 
        }

        if (isNaN(parseFloat($(".hybrid").val()))) {
          hybrid = 0;
        }

        else {
          hybrid = parseFloat($(".hybrid").val()) * kgPerHybrid; 
        }

        if (isNaN(parseFloat($(".conventional").val()))) {
          conventional = 0;
        }

        else {
          conventional = parseFloat($(".conventional").val()) * kgPerConventional; 
        }
        final = ((eco+hybrid+conventional)/907.185).toPrecision(3);
        numOfTrees = (eco+hybrid+conventional)/10;

        $('#co2').val(final);
        $('#trees').val(parseInt(numOfTrees));

      });

      $(".hybrid").on('keyup keydown keypress', function () {
        //alert("called");
        $("#cement").val("");
        $("#steel").val("");
        $("#bricks").val("");
        $("#glass").val("");
        $("#tiles").val("");
        $("#aluminium").val("");


        if (isNaN(parseFloat($(".eco").val()))) {
          eco = 0;
        }

        else {
          eco = parseFloat($(".eco").val()) * kgPerEco; 
        }

        if (isNaN(parseFloat($(".hybrid").val()))) {
          hybrid = 0;
        }

        else {
          hybrid = parseFloat($(".hybrid").val()) * kgPerHybrid; 
        }

        if (isNaN(parseFloat($(".conventional").val()))) {
          conventional = 0;
        }

        else {
          conventional = parseFloat($(".conventional").val()) * kgPerConventional; 
        }
        final = ((eco+hybrid+conventional)/907.185).toPrecision(3);
        numOfTrees = (eco+hybrid+conventional)/10;

        $('#co2').val(final);
        $('#trees').val(parseInt(numOfTrees));

      });

      $(".conventional").on('keyup keydown keypress', function () {
        //alert("called");
        $("#cement").val("");
        $("#steel").val("");
        $("#bricks").val("");
        $("#glass").val("");
        $("#tiles").val("");
        $("#aluminium").val("");
    

        if (isNaN(parseFloat($(".eco").val()))) {
          eco = 0;
        }

        else {
          eco = parseFloat($(".eco").val()) * kgPerEco; 
        }

        if (isNaN(parseFloat($(".hybrid").val()))) {
          hybrid = 0;
        }

        else {
          hybrid = parseFloat($(".hybrid").val()) * kgPerHybrid; 
        }

        if (isNaN(parseFloat($(".conventional").val()))) {
          conventional = 0;
        }

        else {
          conventional = parseFloat($(".conventional").val()) * kgPerConventional; 
        }
        final = ((eco+hybrid+conventional)/907.185).toPrecision(3);
        numOfTrees = (eco+hybrid+conventional)/10;

        $('#co2').val(final);
        $('#trees').val(parseInt(numOfTrees));

      });

    });

  }



ngOnInit(){
  this.productService.getProducts().subscribe(products =>
    {
      console.log("All Products");
      this.products_details= products;
      console.log(this.products_details);
      var oldItems = JSON.parse(localStorage.getItem('itemsArray')) || [];
      console.log('Total Cart Item');
      console.log(oldItems.length);
      this.oldItemsCount = oldItems.length; 
      console.log('Old Item');
      console.log(this.oldItemsCount);
      console.log('Cart Item List');
      console.log(oldItems);
      console.log('Old Item');
      this.oldItemList = oldItems;
    })


    //Products list show / hide
    $(document).ready(function(){
      $('.products-container').hide();
      $('.map-links').click(function(){
        $('.products-container').show();
      });
    });
}




}
