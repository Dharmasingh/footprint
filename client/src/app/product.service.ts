import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http:HttpClient) { }

  getProducts(){
    return this.http.get('http://localhost:3000/products');
  }

  
  getWesternHimalaya(){
    return this.http.get('http://localhost:3000/western-himalaya');
  }

  getPlainofPunjab(){
    return this.http.get('http://localhost:3000/plains-of-punjab');
  }

  getAridWesternZone(){
    return this.http.get('http://localhost:3000/arid-western-zone');
  }

  getUrbanCentres(){
    return this.http.get('http://localhost:3000/urban-centres');
  }

  getWesternGhats(){
    return this.http.get('http://localhost:3000/western-ghats');
  }

  getGaneticPlains(){
    return this.http.get('http://localhost:3000/ganetic-plains');
  }

  getCentralIndia(){
    return this.http.get('http://localhost:3000/central-india');
  }

  getDeccanPlateau(){
    return this.http.get('http://localhost:3000/deccan-plateau');
  }
  getEasternHimalaya(){
    return this.http.get('http://localhost:3000/eastern-himalaya');
  }
  getEasternPlains(){
    return this.http.get('http://localhost:3000/eastern-plains');
  }
  getEasternGhats(){
    return this.http.get('http://localhost:3000/eastern-ghats');
  }
  getSouthernPlains(){
    return this.http.get('http://localhost:3000/southern-plains');
  }



  getPlantedTrees(){
    return this.http.get('http://localhost:3000/planted-trees');
  }

  
}
