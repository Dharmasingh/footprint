import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-mycart',
  templateUrl: './mycart.component.html',
  styleUrls: ['./mycart.component.scss']
  // providers: [ProductService]
})
export class MycartComponent implements OnInit {

  products_details;
  oldItems:any;
  oldItemsCount;
  oldItemList;

  deleteItem;
  removeItem;

  constructor(private productService: ProductService) { }


  ngOnInit() {
    this.productService.getProducts().subscribe(products =>
      {
        console.log("All Products");
        this.products_details= products;
        var oldItems = JSON.parse(localStorage.getItem('itemsArray')) || [];
        console.log('Cart List');
        this.oldItemList = oldItems;
      })
  }

  removeCartItem(index){
    var deleteItem = JSON.parse(localStorage.getItem('itemsArray')) || [];
    this.removeItem = deleteItem[index];
    localStorage.removeItem(JSON.stringify(this.removeItem));
    console.log(this.removeItem);
  }


}
