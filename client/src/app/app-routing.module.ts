import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MycartComponent } from './mycart/mycart.component';
import { ProductdetailsComponent } from './productdetails/productdetails.component'
import { OrdersComponent } from './orders/orders.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'mycart',
    component: MycartComponent,
  },
  {
    path: 'productdetails',
    component: ProductdetailsComponent,
  },
  {
    path: 'orders',
    component: OrdersComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
