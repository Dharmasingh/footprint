import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ProductService } from '../product.service';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [ProductService]
})
export class HeaderComponent implements OnInit {

  planted_trees:any;
  totTree:any;
  tonsCarbon:any;



  oldItemsCount;
  
  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.productService.getPlantedTrees().subscribe(data =>{
      this.planted_trees= data[0];
      
      console.log("Tons of Carbon");
      this.tonsCarbon = this.planted_trees.totTree;
      console.log(this.tonsCarbon);
      
      console.log("Total Planted Trees");
      this.totTree = this.tonsCarbon/10;
      console.log(this.tonsCarbon);
    })


    //Cart Count and List
    this.productService.getProducts().subscribe(products =>
      {
        var oldItems = JSON.parse(localStorage.getItem('itemsArray')) || [];
        console.log('Total Cart Item');
        console.log(oldItems.length);
        this.oldItemsCount = oldItems.length; 
      })
    }



  }





